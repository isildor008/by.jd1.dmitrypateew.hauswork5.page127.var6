import entity.Moon;
import entity.ObjectSistem;
import entity.Planet;

import java.util.Arrays;

/**
 * Created by isild on 26.03.2017.
 */
public class StarSystem {
    private ObjectSistem[] system=new ObjectSistem[3];


    public ObjectSistem[] getSystem() {
        return system;
    }

    public void setSystem(ObjectSistem[] system) {
        this.system = system;
    }

    public ObjectSistem addMoon(){

       ObjectSistem moon = new Moon();
        system[0]=moon;
        return  moon ;
    }

    public ObjectSistem addPlanet(){

        ObjectSistem planet = new Planet();
        ObjectSistem planet2 = new Planet();
        system[1]=planet;
        system[2]=planet2;
        return planet ;
    }

    public int cuntPlanet(ObjectSistem objectSistem){
        int b=0;
        Planet planet=new Planet();
        for(int i=0;i<system.length;i++){
          if (system[i].getClass().equals(Planet.class)){

                      b++;
          }
        }
       return b;
    }




    @Override
    public String toString() {
        return "StarSystem{" +
                "system=" + Arrays.toString(system) +
                '}';
    }
}
