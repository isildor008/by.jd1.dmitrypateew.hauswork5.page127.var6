package entity;

/**
 * Created by isild on 26.03.2017.
 */
public class Star extends ObjectSistem {
    private String name;
    private int mass;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMass() {
        return mass;
    }

    public void setMass(int mass) {
        this.mass = mass;
    }

    @Override
    public String toString() {
        return "Star{" +
                "name='" + name + '\'' +
                ", mass=" + mass +
                '}';
    }


}
