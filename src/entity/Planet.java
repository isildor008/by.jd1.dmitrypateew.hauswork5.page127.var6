package entity;

/**
 * Created by isild on 26.03.2017.
 */
public class  Planet extends ObjectSistem{
    private String name;

    private String people;

    public String getPeople() {
        return people;
    }

    public void setPeople(String people) {
        this.people = people;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Planet{" +
                "name='" + name + '\'' +
                '}';
    }
}
