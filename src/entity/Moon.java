package entity;

/**
 * Created by isild on 26.03.2017.
 */
public class Moon  extends  ObjectSistem{


    public Moon(){
        name="Moon";
    }


    @Override
    public String toString() {
        return "Moon{" +
                "name='" + name + '\'' +
                '}';
    }
}
