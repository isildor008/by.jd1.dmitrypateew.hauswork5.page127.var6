package entity;

/**
 * Created by isild on 26.03.2017.
 */
public  class ObjectSistem {

    protected String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ObjectSistem{" +
                "name='" + name + '\'' +
                '}';
    }
}
